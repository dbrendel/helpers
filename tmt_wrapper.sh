#!/bin/sh

tmt() {
  if test -z "$TMT_IMAGE"; then
    cloud_url="https://cloud.centos.org/centos/9-stream/x86_64/images/"
    image=$(curl --silent $cloud_url | grep -o "CentOS-Stream-GenericCloud-9-202[0-9]\{5\}\.[0-9]"\.x86_64\.qcow2 | tail -1)
    export TMT_IMAGE="$cloud_url$image"
  fi

  pos=1
  for arg in $@; do
    if test "${arg#-}" = "centos-stream9-latest"; then
      set -- "${@:1:$((pos - 1))}" "$TMT_IMAGE" "${@:$((pos + 1)):$#}"
    fi
    pos=$((pos + 1))
  done

  command tmt $@
}

